/*
 * Copyright (c) 2020 By Legion. All rights reserved
 *
 * created by Sebastian Fernandez on 2020-07-27 at 19:52
 */
package edu.app.config;

import java.io.File;
import javax.swing.JFileChooser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class UIConfig {

    @Bean
    public JFileChooser fileChooser(ConfigurationModel configurationModel) {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File(configurationModel.getRawFolder()));
        fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        fileChooser.setMultiSelectionEnabled(true);

        return fileChooser;
    }
}
