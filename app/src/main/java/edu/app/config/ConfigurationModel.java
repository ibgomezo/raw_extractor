package edu.app.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Controller;

@ConfigurationProperties(prefix = "config")
@Controller
public class ConfigurationModel {

	@Getter @Setter private Boolean showDicomImage;

	@Getter @Setter private Boolean processAutomatically;

	@Getter @Setter private String rawFolder;

}
