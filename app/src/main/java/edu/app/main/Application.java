/*
 * Copyright (c) 2020 By Legion. All rights reserved
 *
 * created by Sebastian Fernandez on 2020-07-27 at 19:36
 */
package edu.app.main;

import edu.app.view.MainFrame;
import javax.swing.SwingUtilities;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(value = "edu")
public class Application {
    /**
     * Auto-generated main method to display this JFrame
     */
    public static void main(String[] args) {
        // ApplicationContext ctx = SpringApplication.run(Application.class, args);
        SpringApplicationBuilder builder = new SpringApplicationBuilder(Application.class);
        builder.headless(false);
        ConfigurableApplicationContext ctx = builder.run(args);
        MainFrame mainFrame = ctx.getBean(MainFrame.class);
        mainFrame.init();
        SwingUtilities.invokeLater(mainFrame::init);
    }
}
