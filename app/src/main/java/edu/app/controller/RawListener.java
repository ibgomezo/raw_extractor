/*
 * Copyright (c) 2020 By Legion. All rights reserved
 *
 * created by Sebastian Fernandez on 2020-07-16 at 19:32
 */
package edu.app.controller;

public interface RawListener {

    void valueChange(String valueName, int value);
}
