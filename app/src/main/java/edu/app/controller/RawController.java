/*
 * Copyright (c) 2020 By Legion. All rights reserved
 *
 * created by Sebastian Fernandez on 2020-08-27 at 20:15
 */
package edu.app.controller;

import edu.app.config.ConfigurationModel;
import edu.app.view.DataFrame;
import edu.app.view.PreviewFrame;
import edu.app.view.RawView;
import edu.core.dicom.DicomImage;
import edu.core.raw.process.search.RawFinder;
import edu.core.raw.RawProcessor;
import edu.core.raw.image.RawImage;
import edu.core.raw.process.algorithm.AlgorithmConfig;
import edu.core.raw.process.solver.ConfigResolver;
import ij.ImagePlus;
import ij.plugin.DICOM;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RawController implements RawListener {
    
    @Autowired 
    private RawProcessor rawProcessor;

    @Autowired
    private RawFinder rawFinder;

    @Autowired
    @Getter
    private AlgorithmConfig algorithmConfig;

    @Autowired
    private ConfigurationModel configurationModel;

    @Autowired
    private ConfigResolver configResolver;
    
    public static final String WIDTH = "Ancho";
    public static final String HEIGHT = "Alto";
    public static final String OFFSET = "Desplazamiento";
    public static final String GAP = "Espacio entre cada imagen";

    private RawView view;

    @Getter
    private List<PreviewFrame> previewFrames;

    public RawController() {
        this.view = new RawView(this);
        previewFrames = new ArrayList<>();
    }

    

    public void setHip(boolean hip) {
        rawProcessor.getRawConfig().setHip(hip);
        this.openRaw(false);
    }

    /**
     * Open a RAW Image
     **/
    public void openRaw(boolean showInfo) {
        view.showRaw(rawProcessor.getRawImage());
        if (showInfo)
            view.loadPatientDataFromRaw();
    }

    public RawView openRawImage(File file) {
        this.setFile(file);
        return view;
    }

    public RawView openRawFromDicom() {
        if (configurationModel.getProcessAutomatically()) {
            this.setFile(rawFinder.openRawFromDicom(rawProcessor.getDicomImage(), configurationModel.getRawFolder()));
            configResolver.setConfig(rawProcessor.getDxaImage());
            rawProcessor.updateRaw();
        }

        return view;
    }


    public void changeField(String valueName, int value) {
        switch (valueName) {
            case WIDTH:
                rawProcessor.getRawConfig().setWidth(value);
                break;
            case HEIGHT:
                rawProcessor.getRawConfig().setHeight(value);
                break;
            case OFFSET:
                rawProcessor.getRawConfig().setOffset(value);
                break;
            case GAP:
                rawProcessor.getRawConfig().setGap(value);
                break;
            default:
                break;
        }
        if (value != 0) {
            this.openRaw(false);
        }
    }

    public void loadPatientDataFromDcm(File img) {
        try {
            if(img.getName().endsWith("dcm")){
                InputStream iStream = new FileInputStream(img);
                DICOM dcm = new DICOM(iStream);
                dcm.run(img.getName());
                dcm.getOriginalFileInfo();
                DataFrame dFrame = new DataFrame("Informacion", 300, 500);
                this.rawProcessor.openDicom(dcm);
                this.rawProcessor.getDicomImage().setPath(img.getAbsolutePath());
                for(String key: rawProcessor.getDicomImage().getKeySet()) {
                    String nline = key + ": " + rawProcessor.getDicomImage().get(key);
                    dFrame.addTextLine(nline);
                }
                dFrame.showWindow();
                dcm.show();
            } else {

            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void extractAndOpenRawImages() {
        this.rawProcessor.getRawImage().setWidth(rawProcessor.getRawConfig().getWidth());
        int _width = rawProcessor.getRawConfig().getWidth();
        int _height = rawProcessor.getRawConfig().getHeight();
        int _offset = rawProcessor.getRawConfig().getOffset();
        int _gap = rawProcessor.getRawConfig().getGap();
        PreviewFrame  frame1 = new PreviewFrame("Image1", _width, _height,
            rawProcessor.getRawImage().getBytesFromImageOne(_height, _offset), rawProcessor.getFile().getAbsolutePath());
        frame1.setAlgorithm(algorithmConfig.getAlgorithms());

        PreviewFrame  frame2 = new PreviewFrame("Image2", _width, _height,
            rawProcessor.getRawImage().getBytesFromImageTwo(_height, _offset, _gap), rawProcessor.getFile().getAbsolutePath());
        frame2.setAlgorithm(algorithmConfig.getAlgorithms());

        frame1.setBrother(frame2);
        frame2.setBrother(frame1);
        frame1.setRawController(this);
        frame2.setRawController(this);
    }


    public void extractImg1(ImagePlus imagePlus, int imgNumber) {
        PreviewFrame pframe = new PreviewFrame("Image " + imgNumber, imagePlus.getStack().getProcessor(imgNumber), rawProcessor.getFile().getAbsolutePath());
        pframe.setRawController(this);
        pframe.addWindowListener(new WindowListener() {
            @Override
            public void windowOpened(WindowEvent e) {

            }

            @Override
            public void windowClosing(WindowEvent e) {

            }

            @Override
            public void windowClosed(WindowEvent e) {
                PreviewFrame frame = (PreviewFrame)e.getSource();
                previewFrames.remove(frame);
            }

            @Override
            public void windowIconified(WindowEvent e) {

            }

            @Override
            public void windowDeiconified(WindowEvent e) {

            }

            @Override
            public void windowActivated(WindowEvent e) {

            }

            @Override
            public void windowDeactivated(WindowEvent e) {

            }
        });
        pframe.setAlgorithm(algorithmConfig.getAlgorithms());
        pframe.setBrother(null);
        previewFrames.add(pframe);
    }


    public DicomImage getDicom() {
        return this.rawProcessor.getDicomImage();
    }

    @Override
    public void valueChange(String valueName, int value) {
        changeField(valueName, value);
    }

    public void setFile(File file) {
        rawProcessor.setFile(file);
        openRaw(false);
    }

    public RawImage getRawImage(){
        return rawProcessor.getRawImage();
    }

    public Map<String, String> getDataFromRaw() {
        return rawProcessor.getDataFromRaw();
    }
}
