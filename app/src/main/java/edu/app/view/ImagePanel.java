package edu.app.view;

import java.awt.Dimension;
import java.awt.Graphics;

import javax.swing.JPanel;

import ij.ImagePlus;
import javax.swing.JScrollPane;
import lombok.Getter;
import lombok.Setter;

public class ImagePanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Getter @Setter private ImagePlus raw;
	@Getter @Setter private ImagePlus img;

	private JScrollPane scrollPane;

	public JScrollPane getPanel() {
		return scrollPane;
	}

	public ImagePanel() {
		this.raw = null;
		this.img = null;

		scrollPane = new JScrollPane();
		scrollPane.setPreferredSize(new Dimension(900, 700));
		scrollPane.setViewportView(this);
	}
	
	@Override  
    protected void paintComponent(Graphics g) {  
        super.paintComponent(g);

		if (raw != null) {
			int width = raw != null ? raw.getWidth(): 0; // Ver que carajo pasa con el 0

			g.drawImage(raw.getImage(), 0, 0, null);
			if (img != null) {
				g.drawImage(img.getStack().getProcessor(1).getBufferedImage(), width + 10, 0, null);
				g.drawImage(img.getStack().getProcessor(2).getBufferedImage(), width * 2 + 20, 0, null);
			}
		}
    }
	
	private void _display() {
		int totalWidth = raw.getWidth() * 3 + 30;
		if (totalWidth < 900) {
			totalWidth = 900;
		}
		this.setPreferredSize(new Dimension(totalWidth, raw.getHeight()));
		scrollPane.setViewportView(this);
		this.repaint();	
	}

	public void displayImage(ImagePlus raw, ImagePlus img) {
		this.setRaw(raw);
		this.setImg(img);
		_display();
	}

	public void displayImage(ImagePlus raw) {
		this.setRaw(raw);
		this.setImg(null);
		_display();
	}

}
