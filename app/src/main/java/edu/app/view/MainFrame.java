package edu.app.view;

import edu.app.event.Command;
import edu.app.event.RawUIEvent;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.WindowConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class MainFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Autowired
	private ApplicationEventPublisher applicationEventPublisher;


	public MainFrame() {
		super();
	}

	public void init() {
		setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		setJMenuBar(createMenuBar());
		this.setPreferredSize(new Dimension(1200,600));
		this.setVisible(true);
		pack();
	}

	private JMenuBar createMenuBar(){
		final JMenuBar menuBar = new JMenuBar();

		menuBar.add(createMenu("Archivo",
				Arrays.asList(
						createMenuItem("Abrir Raw" , Command.OpenRaw),
						createMenuItem("Abrir Dicom", Command.OpenDicom)),
				true));

		menuBar.add(createMenu("Configuración",
				Collections.singletonList(createMenuItem("Editar", Command.OpenConfig)),
			true));// __ es para parametros sin uso
		
		return menuBar;
	}

	private JMenu createMenu(String name, List<JMenuItem> menus, boolean enable){

		final JMenu menu = new JMenu(name);
		menus.forEach(menu::add);
		menu.setEnabled(enable);

		return menu;
	}

	private JMenuItem createMenuItem(String text, String command){
		final JMenuItem menu = new JMenuItem();
		menu.setText(text);
		menu.setActionCommand(command);
		menu.addActionListener(this::triggerEvent);
		return  menu;
	}

	private void triggerEvent(ActionEvent evt) {
		applicationEventPublisher.publishEvent(new RawUIEvent(evt));
	}

	public void showView(RawView inst){
		this.getContentPane().removeAll();
		this.add(inst);
		inst.setVisible(true);
		pack();
	}
}
