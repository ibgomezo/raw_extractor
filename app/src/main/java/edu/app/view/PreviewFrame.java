package edu.app.view;

import edu.app.controller.RawController;
import edu.core.raw.process.algorithm.Algorithm;
import edu.core.util.Utils;
import ij.ImagePlus;
import ij.io.FileSaver;
import ij.process.ByteProcessor;
import ij.process.ImageConverter;
import ij.process.ImageProcessor;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.UIManager;
import javax.swing.WindowConstants;


public class PreviewFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private PreviewFrame brother;
	private RawController rawController;
	
	private JMenuBar menubar;
	private JMenu file;
	private JMenuItem i_expTiff;
	private JMenuItem i_expJpeg;
	private JMenuItem i_expRaw;
	private JMenuItem i_expBmp;
	
	private JMenu edit;
	private JMenuItem i_rotLeft;
	private JMenuItem i_rotRight;
	private JPanel mainPanel;
		
	private PreviewPanel imgPanel;
	// private ImagePlus imgPlus;
	
	private ObserverProgressBar progressBar;
	
	private FileSaver fileSaver ;
	byte[] bytes;
	private String rawPath;
	ImageConverter converter;

	public PreviewFrame(String title, int width, int height, byte[] bytes, String rawPath) {
		super(title);
		this.bytes = bytes;
		this.rawPath = rawPath;
		ByteProcessor byteProcessor = new ByteProcessor(width, height, bytes, null);
		ImagePlus img = new ImagePlus("My Image", byteProcessor);
		img.deleteRoi();;
		init(img);
	}
	
	public PreviewFrame(String title, ImagePlus img) {
		super(title);
		init(img);
	}

	public PreviewFrame(String title, ImageProcessor ip, String rawPath) {
		super(title);
		this.rawPath = rawPath;
		ImagePlus img = new ImagePlus(title, ip);
		img.deleteRoi();
		init(img);
	}
	
	private void init(ImagePlus img) {
		UIManager.put("ObserverProgressBar.background", Color.ORANGE);
		UIManager.put("ObserverProgressBar.foreground", Color.BLUE);
		UIManager.put("ObserverProgressBar.selectionBackground", Color.RED);
		UIManager.put("ObserverProgressBar.selectionForeground", Color.GREEN);
		
		fileSaver = new FileSaver(img);
		//converter = new ImageConverter(img);
		//converter.convertToGray16();
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

		//Barra de Menú
		menubar = new JMenuBar();
		this.setJMenuBar(menubar);

		//Menu Archivo
		file = new JMenu("Archivo");

		i_expTiff = new JMenuItem();
		i_expTiff.setText("Exportar como TIFF");
		i_expTiff.addActionListener(e -> fileSaver.saveAsTiff());
		file.add(i_expTiff);

		i_expJpeg = new JMenuItem();
		i_expJpeg.setText("Exportar como JPEG");
		i_expJpeg.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				FileSaver.setJpegQuality(100);
				fileSaver.saveAsJpeg();
			}
		});
		file.add(i_expJpeg);

		i_expRaw = new JMenuItem();
		i_expRaw.setText("Exportar como RAW");
		i_expRaw.addActionListener(this::exportAsRaw);
		file.add(i_expRaw);

		i_expBmp = new JMenuItem();
		i_expBmp.setText("Exportar como BMP");
		i_expBmp.addActionListener(e -> fileSaver.saveAsBmp());
		file.add(i_expBmp);

		i_expBmp = new JMenuItem();
		i_expBmp.setText("Exportar con Roi");
		FileSaver fs2 = new FileSaver(img.crop());
		i_expBmp.addActionListener(e -> fs2.saveAsJpeg());
		file.add(i_expBmp);

		menubar.add(file);
		
		//Menu Editar
		edit = new JMenu("Editar");
		i_rotLeft = new JMenuItem();
		i_rotLeft.setText("Rotar a la izquierda");
		i_rotLeft.addActionListener(this::rotateLeft);
		edit.add(i_rotLeft);

		i_rotRight = new JMenuItem();
		i_rotRight.setText("Rotar a la derecha");
		i_rotRight.addActionListener(this::rotateRight);
		edit.add(i_rotRight);

		menubar.add(edit);
		
		//Panel de la imagen
		imgPanel = new PreviewPanel();
		imgPanel.setImg(img);
		imgPanel.setPreferredSize(new Dimension(img.getWidth(), img.getHeight()));
		
		mainPanel = new JPanel();
		mainPanel.setPreferredSize(new Dimension(img.getWidth(), img.getHeight() + 3));
		
		mainPanel.add(imgPanel);
		
		progressBar = new ObserverProgressBar(img.getWidth(), 3);
		mainPanel.add(progressBar);	
		
		this.add(mainPanel);
		
		// Valores de la ventana
		setPreferredSize(new Dimension(mainPanel.getPreferredSize().width + 8 , mainPanel.getPreferredSize().height + menubar.getPreferredSize().height + 45));
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.pack();
	}

	public void setAlgorithm(List<Algorithm> algorithms) {

		List<JMenuItem> result = new ArrayList<>();

		for(Algorithm algorithm: algorithms) {

			result.add(createMenuItem(algorithm.getName(), e -> {
				applyAlgorithm(algorithm);
			}));
		}
		menubar.add(createMenu("Algoritmos", result));
	}

	private void applyAlgorithm(Algorithm algorithm) {
		algorithm.addObserver(progressBar);
		// ImagePlus sister = brother != null ? brother.getImagePanel().getImg() : null;
		// ImagePlus dicom = rawController.getDicom() != null ? rawController.getDicom().getSource() : null;
		// String dcmPath = dicom != null ? rawController.getDicom().getPath() : null;

		// ImagePlus high = new ImagePlus("HIGH EN",rawController.getRawImage().getImages()
		// 	.getStack().getProcessor(1));
		// high.setRoi(imgPlus.getRoi());
		// ImagePlus low = new ImagePlus("LOW EN", rawController.getRawImage().getImages()
		// 	.getStack().getProcessor(2));
		// low.setRoi(imgPlus.getRoi());
		// ImagePlus ip = algorithm.apply(high, low, rawPath);

		ImagePlus self = new ImagePlus("SELF", this.getImagePlus().getProcessor());
		self.setRoi(this.getImagePlus().getRoi());
		ImagePlus sister = null;
		if (this.brother != null) {
			sister = new ImagePlus("SISTER", this.brother.getImagePanel().getImg().getProcessor());
			sister.setRoi(this.getImagePlus().getRoi());
		}
		ImagePlus ip = algorithm.apply(self, sister, rawPath);

		if (ip != null) {
			PreviewFrame previewFrame = new PreviewFrame(ip.getTitle(), ip);
			previewFrame.setAlgorithm(rawController.getAlgorithmConfig().getAlgorithms());
			previewFrame.setRawController(rawController);
		} else {
			// Mostrar un mensaje que no hubo resultados
			JOptionPane.showMessageDialog(null, "Sin resultados para mostrar");
		}
		// this.imgPanel.displayImage(ip);
	}

	public void setBrother(PreviewFrame brother) {
		this.brother = brother;
	}
	
	public void setRawController(RawController proc) {
		this.rawController = proc;
	}
	
	public PreviewPanel getImagePanel() {
		return this.imgPanel;
	}

	public ImagePlus getImagePlus() {
		return this.imgPanel.getImg();
	}
	

	/**
	 * Esta exportación se hace manual, porque no está funcionando FileSaver.saveAsRaw()
	 * Crea un archivo vacío
	 **/
	private void exportAsRaw(ActionEvent evt) {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File("./"));
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setMultiSelectionEnabled(true);

		if(fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
			File file=fileChooser.getSelectedFile();
			try (FileOutputStream fileOutputStream = new FileOutputStream(file)) {
				if(bytes == null) {
					bytes = Utils.getBytesFromImagPlus(this.getImagePlus());
				}
				fileOutputStream.write(bytes);
				fileOutputStream.flush();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	private void repaintImage() {
		imgPanel.setPreferredSize(new Dimension(this.getImagePlus().getWidth(), this.getImagePlus().getHeight()));
		progressBar.setPreferredSize(new Dimension(this.getImagePlus().getWidth(), 3));
		mainPanel.setPreferredSize(new Dimension(this.getImagePlus().getWidth(), this.getImagePlus().getHeight() + 3));
		setPreferredSize(new Dimension(mainPanel.getPreferredSize().width + 8 , mainPanel.getPreferredSize().height + menubar.getPreferredSize().height + 45));
		this.pack();
		imgPanel.displayImage(this.getImagePlus());
	}

	private void rotateLeft(ActionEvent evt) {
		this.getImagePlus().setProcessor(this.getImagePlus().getProcessor().rotateLeft());
		repaintImage();
	}

	private void rotateRight(ActionEvent evt) {
		this.getImagePlus().setProcessor(this.getImagePlus().getProcessor().rotateRight());
		repaintImage();
	}
	
	private JMenu createMenu(String name, List<JMenuItem> menus){
		final JMenu menu = new JMenu(name);
		menus.forEach(menu::add);
		return menu;
	}
	
	private JMenuItem createMenuItem(String text, ActionListener al){
		final JMenuItem menu = new JMenuItem();
		menu.setText(text);
		menu.addActionListener(al);
		return  menu;
	}

	@Override
	public String toString() {
		return this.getTitle();
	}


	final static class ObserverProgressBar extends JProgressBar implements Observer {

		/**
		 *
		 */
		private static final long serialVersionUID = 1L;

		public ObserverProgressBar(int width, int height) {
			setPreferredSize(new Dimension(width, height));
		}

		@Override
		public void update(Observable o, Object arg) {
			Integer value = (Integer) arg;
			this.setValue(value);
			if(getGraphics() != null)
				update(getGraphics());
		}
	}
}


