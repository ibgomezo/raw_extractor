/*
 * Copyright (c) 2020 By Legion. All rights reserved
 *
 * created by Sebastian Fernandez on 2020-06-22 at 18:09
 */
package edu.app.view;

import ij.ImagePlus;
import ij.gui.PolygonRoi;
import ij.gui.Roi;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.Vector;
import javax.swing.JPanel;
import lombok.Getter;
import lombok.Setter;

public class PreviewPanel extends JPanel {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Getter @Setter private ImagePlus img;

    private Vector<Point> roiPoints;
    private boolean isDragging;
    private boolean isDrawingPolygon;
    private String mousePosition;

    public PreviewPanel() {
        this.img = null;
        isDragging = false;
        isDrawingPolygon = false;
        roiPoints = new Vector<>();
        mousePosition = "X:0, Y:0";


        this.addMouseListener(new MouseListener() {
            @Override
            public void mouseReleased(MouseEvent e) {
                if(e.getButton() == MouseEvent.BUTTON1 && isDragging) {
                    roiPoints.add(new Point(e.getX(), e.getY()));
                    img.setRoi(new Rectangle(
                        roiPoints.get(0).x,
                        roiPoints.get(0).y,
                        roiPoints.get(1).x - roiPoints.get(0).x,
                        roiPoints.get(1).y - roiPoints.get(0).y
                    ));
                    _display();
                    isDragging = false;
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
                if(e.getButton() == MouseEvent.BUTTON3) {
                    if(isDrawingPolygon) {
                        roiPoints.add(new Point(e.getX(), e.getY()));
                        isDrawingPolygon = false;
                    } else {
                        roiPoints = new Vector<Point>();
                        isDrawingPolygon = false;
                        isDragging = false;
                        img.deleteRoi();
                    }
                    _display();
                } else if(e.getButton() == MouseEvent.BUTTON1) {
                    roiPoints.add(new Point(e.getX(), e.getY()));
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseEntered(MouseEvent e) {
                // TODO Auto-generated method stub

            }

            @Override
            public void mouseClicked(MouseEvent e) {
                if(e.getButton() == MouseEvent.BUTTON1)
                    isDrawingPolygon = true;
            }
        });

        this.addMouseMotionListener(new MouseMotionListener() {

            @Override
            public void mouseMoved(MouseEvent e) {
                mousePosition = "X:"+e.getX()+", Y:"+e.getY();
                if(isDrawingPolygon) {
                    float[] xs = new float[roiPoints.size() + 1];
                    float[] ys = new float[roiPoints.size() + 1];
                    int i=0;
                    for(; i<roiPoints.size(); i++) {
                        xs[i] = roiPoints.get(i).x;
                        ys[i] = roiPoints.get(i).y;
                    }
                    xs[i] = e.getX();
                    ys[i] = e.getY();
                    if(xs.length > 1) {
                        img.setRoi(new PolygonRoi(xs,ys, Roi.FREEROI));
                    }
                }
                _display();
            }

            @Override
            public void mouseDragged(MouseEvent e) {
                if(roiPoints.size() == 1) {
                    isDragging = true;
                    img.setRoi(new Rectangle(
                        roiPoints.get(0).x,
                        roiPoints.get(0).y,
                        e.getX() - roiPoints.get(0).x,
                        e.getY() - roiPoints.get(0).y
                    ));
                    _display();
                }
            }
        });

    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        g.drawImage(img.getImage(), 0, 0, null);
        g.setColor(Color.YELLOW);

        drawPolygon(g);
		g.drawString(mousePosition, 0, 10);

		if(img.getRoi() != null) {
			int npoints = img.getRoi().getPolygon().npoints;
			for(int i=0; i<npoints; i++) {
				g.drawString(
						"X:"+(img.getRoi().getPolygon().xpoints)[i]+",Y:"+(img.getRoi().getPolygon().ypoints)[i],
						img.getWidth() - 80,
						12 * (i+1)
				);
			}
		}

    }

    private void drawPolygon(Graphics g) {
        if(img.getRoi() != null){
            g.drawPolygon(img.getRoi().getPolygon());
        }
    }

    private void resize() {
        this.setPreferredSize(new Dimension(img.getWidth(), img.getHeight()));
    }

    private void _display() {
        resize();
        this.repaint();
    }

    public void displayImage(ImagePlus img) {
        this.setImg(img);
        _display();
    }
}
