package edu.app.view;

import edu.app.config.ConfigurationModel;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextField;

public class ConfigurationFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private JCheckBox chckbxMostrarImagenDicom;
	private JCheckBox chckbxProcesarArchivosAutomaticamente;
	private JTextField textField;


	private ConfigurationModel model;
	
	public ConfigurationFrame(ConfigurationModel model) {
		this.model = model;
		getContentPane().setLayout(new GridLayout(1, 0, 0, 0));
		getContentPane().setPreferredSize(new Dimension(500, 300));
		this.setLocationRelativeTo(null);
		this.setTitle("Configuración");
		
		JPanel panel = new JPanel();
		getContentPane().add(panel);
		panel.setLayout(null);
		
		chckbxMostrarImagenDicom = new JCheckBox("Mostrar imagen DICOM al abrir");
		chckbxMostrarImagenDicom.setBounds(43, 31, 255, 23);
		// panel.add(chckbxMostrarImagenDicom);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(32, 12, 420, 2);
		panel.add(separator);
		
		chckbxProcesarArchivosAutomaticamente = new JCheckBox("Intentar abrir raw al abrir DICOM");
		chckbxProcesarArchivosAutomaticamente.setBounds(43, 31, 255, 23);//(43, 58, 283, 23);
		panel.add(chckbxProcesarArchivosAutomaticamente);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(32, 70, 420, 2);
		panel.add(separator_1);
		
		JLabel lblCarpetaContenedoraDe = new JLabel("CARPETA CONTENEDORA DE RAWS");
		lblCarpetaContenedoraDe.setBounds(43, 111, 255, 15);
		panel.add(lblCarpetaContenedoraDe);
		
		textField = new JTextField();
		textField.setBounds(43, 138, 299, 19);
		panel.add(textField);
		textField.setColumns(10);
		
		JButton btnCambiar = new JButton("Cambiar");
		btnCambiar.setBounds(354, 135, 114, 25);
		btnCambiar.addActionListener(this::changeFolder);
		panel.add(btnCambiar);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(354, 217, 114, 25);
		btnGuardar.addActionListener(this::save);
		panel.add(btnGuardar);
		
		load();
		
	}
	
	private void load() {
		chckbxMostrarImagenDicom.setSelected(model.getShowDicomImage());
		chckbxProcesarArchivosAutomaticamente.setSelected(model.getProcessAutomatically());
		textField.setText(model.getRawFolder());
	}
	
	private void changeFolder(ActionEvent evt) {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setCurrentDirectory(new File(model.getRawFolder()));
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		fileChooser.setMultiSelectionEnabled(false);
		if (fileChooser.showOpenDialog(this) == JFileChooser.APPROVE_OPTION) {
			textField.setText(fileChooser.getSelectedFile().getAbsolutePath());	
			textField.repaint();
		}
	}
	
	private void save(ActionEvent evt) {
		model.setProcessAutomatically(chckbxProcesarArchivosAutomaticamente.isSelected());
		model.setShowDicomImage(chckbxMostrarImagenDicom.isSelected());
		model.setRawFolder(textField.getText());
		this.dispose();
	}
	
	public void showWindow() {
		this.setVisible(true);
		this.pack();
	}
}
