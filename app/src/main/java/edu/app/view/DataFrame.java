package edu.app.view;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.WindowConstants;

public class DataFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String text = new String(); 
	
	private JMenu file;
	private JMenuItem i_expTxt;
	private JScrollPane scrollPane;
	private JTextPane textPanel;
	
	public DataFrame(String title, int width, int height){
		super(title);
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		//Barra de Menú
		JMenuBar menubar = new JMenuBar();
		this.setJMenuBar(menubar);	
		//Menu Archivo
		file = new JMenu("Archivo");
		//Item Guardar
		i_expTxt = new JMenuItem();
		i_expTxt.setText("Guardar");
		i_expTxt.addActionListener(this::saveToTxt);
		file.add(i_expTxt);
		menubar.add(file);	
		
		textPanel = new JTextPane();
		textPanel.setPreferredSize(new Dimension(width, height));
		textPanel.setEditable(false);
		textPanel.setText(this.text);
		//scrollPane.setViewportView(textPanel);
		scrollPane = new JScrollPane(textPanel);
		scrollPane.setPreferredSize(new Dimension(width, height));
		
		//scrollPane.add(textPanel);
		this.getContentPane().add(scrollPane);
		// Valores de la ventana
		this.getContentPane().setPreferredSize(new Dimension(width, height));
		this.setLocationRelativeTo(null);
		
	}
	
	public void addTextLine(String txt) {
		this.text += txt + "\n";		
	}
	
	public void showWindow() {
		this.textPanel.setText(this.text);
		this.setVisible(true);
		this.pack();
	}
	
	private void saveToTxt(ActionEvent evt) {
	    JFileChooser chooser = new JFileChooser();
	    chooser.setCurrentDirectory(new File("."));
	    chooser.showSaveDialog(null);
	    try(FileWriter fw = new FileWriter(chooser.getSelectedFile()+".txt")) {
	        fw.write(text);
	        fw.close();
	    } catch (IOException e) {
			e.printStackTrace();
		}
	    
	}
}
