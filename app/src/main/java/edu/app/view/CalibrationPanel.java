package edu.app.view;

import edu.app.controller.RawListener;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridLayout;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class CalibrationPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private final static int VARIACION_MAYOR = 20;
	private final static int VARIACION_MENOR = 1;
	private final static int BUTTON_WIDTH = 65;
	private final static int BUTTON_HEIGHT = 25;

	private JLabel title;
	protected JTextField value;


	protected Dimension buttonDim = new Dimension(BUTTON_WIDTH, BUTTON_HEIGHT);
	protected JPanel south = new JPanel();
	protected Font bfont = new Font("Dialog", Font.BOLD, 10);

	List<RawListener> listeners;


	public CalibrationPanel(String text, boolean horizontal) {
		super();
		listeners = new ArrayList<>();

		final Dimension panelDim;
		if(!horizontal) {
			panelDim = new Dimension(100, 195);
		} else {
			panelDim = new Dimension(450, 70);
		}
		setPreferredSize(panelDim);
		
		JPanel north = new JPanel();
		if(horizontal) {
			north.setPreferredSize(new Dimension(((Double)(panelDim.getWidth()*0.95)).intValue(), ((Double)(panelDim.getHeight()*0.3)).intValue()));	
		} else {
			north.setPreferredSize(new Dimension(((Double)(panelDim.getWidth()*0.95)).intValue(), ((Double)(panelDim.getHeight()*0.15)).intValue()));
		}
		title = new JLabel(text);		
		north.add(title);
		south.setLayout(new GridLayout(1,7,5,0));

		// south.setPreferredSize(new Dimension(300, 50));
		if(horizontal) {
			south.setPreferredSize(new Dimension(((Double)(panelDim.getWidth()*0.95)).intValue(), BUTTON_HEIGHT));
		} else {
			south.setPreferredSize(new Dimension(((Double)(panelDim.getWidth()*0.95)).intValue(), ((Double)(panelDim.getHeight()*0.85)).intValue()));
		}

		south.add(createButton(buttonDim, bfont, VARIACION_MAYOR, false,"-"+VARIACION_MAYOR));
		south.add(createButton(buttonDim, bfont, VARIACION_MENOR, false, "-"+VARIACION_MENOR));
		
		value = new JTextField();
		value.setHorizontalAlignment(JTextField.CENTER);
		value.setColumns(4);
		value.setPreferredSize(new Dimension(40, BUTTON_HEIGHT));
		value.setEditable(true);
		value.addActionListener(e -> changeField(Integer.parseInt(e.getActionCommand())));
		south.add(value);

		south.add(createButton(buttonDim, bfont, VARIACION_MENOR, true, "+"+VARIACION_MENOR));
		south.add(createButton(buttonDim, bfont, VARIACION_MAYOR, true, "+"+VARIACION_MAYOR));

		this.add(north);
		this.add(south);
		this.value.setText("0");
	}

	protected JButton createButton(Dimension dimension, Font font, int value, boolean sum, String text){
		JButton button = new JButton(text);
		button.setFont(font);
		button.setPreferredSize(dimension);
		button.addActionListener( __ -> changeValue(value, sum));
		return button;
	}

	
	protected void changeValue(int value, boolean sum) {
		value *= sum ? 1:-1;
		int i = Integer.parseInt(this.value.getText()) + value;
		changeField(i);
	}

	public void setValue(Integer v) {
		this.value.setText(v.toString());
	}

	public JLabel getTitle() {
		return title;
	}

	protected void changeField(int value) {
		listeners.forEach(l -> l.valueChange(this.title.getText(), value));
	}

	public void addListener(RawListener listener) {
		listeners.add(listener);
	}
}
