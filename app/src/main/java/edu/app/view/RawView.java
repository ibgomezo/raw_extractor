package edu.app.view;

import edu.app.controller.RawController;
import edu.core.raw.image.RawImage;
import edu.core.raw.image.RawImage.RawPart;
import ij.ImagePlus;
import ij.io.FileSaver;
import ij.process.ByteProcessor;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Optional;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import lombok.Getter;


public class RawView extends JPanel {

    private static final long serialVersionUID = 1L;

    private RawController rawController;

    @Getter
    private ImagePanel imagePanel;

    private CalibrationPanel widthPanel;
    private CalibrationPanel heightPanel;
    private CalibrationPanel offsetPanel;
    private CalibrationPanel gapPanel;

    public RawView(RawController rawController) {
        super();
        this.setLayout(new BorderLayout());
        this.rawController = rawController;
        init();
    }

    private void init() {
        this.setPreferredSize(new Dimension(1200,600));
        this.add(createImagePanel(), BorderLayout.CENTER);
        this.add(createControlsPanel(), BorderLayout.EAST);
        this.setVisible(true);
    }

	private CalibrationPanel createAndSetCalibrationPanel(String name, JPanel parentPanel, boolean horizontal) {
        final CalibrationPanel calibrationPanel = new CalibrationPanel(name, horizontal);
        calibrationPanel.addListener(rawController);
        parentPanel.add(calibrationPanel);
        return calibrationPanel;
    }

    private CalibrationPanel createAndSetOffsetCalibrationPanel(String name, JPanel parentPanel, boolean horizontal) {
        final CalibrationPanel calibrationPanel = new OffsetCalibrationPanel(name, horizontal);
        calibrationPanel.addListener(rawController);
        parentPanel.add(calibrationPanel);
        return calibrationPanel;
    }

    private void addCheckBox(JPanel manualPanel){
        JCheckBox chckbx_isHip = new JCheckBox("Es cadera");
        chckbx_isHip.setPreferredSize((new Dimension(490,30)));
        chckbx_isHip.setSelected(true);
        chckbx_isHip.addActionListener(e -> rawController.setHip(((JCheckBox) e.getSource()).isSelected()));
        manualPanel.add(chckbx_isHip);
    }

    private void addCalibration(JPanel manualPanel) {
        this.widthPanel = createAndSetCalibrationPanel(RawController.WIDTH, manualPanel,true);
        this.heightPanel = createAndSetCalibrationPanel(RawController.HEIGHT, manualPanel, true);
        this.offsetPanel = createAndSetOffsetCalibrationPanel(RawController.OFFSET, manualPanel,true);
        this.gapPanel = createAndSetCalibrationPanel(RawController.GAP, manualPanel,true);
    }

    private void addButtons(JPanel manualPanel) {
        JPanel panel1 = new JPanel();
        panel1.setPreferredSize((new Dimension(480,50)));
        panel1.add(createButton("Extraer Imagen 1", this::extractAndOpenImage, "1"));
        panel1.add(createButton("Extraer Imagen 2", this::extractAndOpenImage, "2"));
        manualPanel.add(panel1);


        panel1 = new JPanel();
        panel1.setPreferredSize((new Dimension(480,50)));
        panel1.add(createButton("Extraer ambas imágenes", this::extractAndOpenImages, "all images"));
        panel1.add(createButton("Guardar en carpeta", this::exportarTodo, "all data"));
        manualPanel.add(panel1);
    }

    private JComponent createControlsPanel(){
        final JPanel controlsPanel = new JPanel();
        controlsPanel.setPreferredSize(new Dimension(500, 600));

        final JPanel manualPanel = new JPanel();
        manualPanel.setPreferredSize(new Dimension(490, 800));
        TitledBorder manual = BorderFactory.createTitledBorder("Extracción manual");
        manualPanel.setBorder(manual);

        addCheckBox(manualPanel);
        addCalibration(manualPanel);
        addButtons(manualPanel);

        controlsPanel.add(manualPanel);
        return controlsPanel;
    }

    private JComponent createImagePanel(){
        this.imagePanel = new ImagePanel();
        return imagePanel.getPanel();
    }

    private JButton createButton(String text, ActionListener al, String name){
        final JButton button = new JButton();
        button.setText(text);
        button.setName(name);
        button.addActionListener(al);

        return button;
    }

    public CalibrationPanel getWidthPanel() {
    	return this.widthPanel;
    }

    public CalibrationPanel getHeightPanel() {
    	return this.heightPanel;
    }

    public CalibrationPanel getOffsetPanel() {
    	return this.offsetPanel;
    }
    
    /****OPERATIONS***/
    
    public void loadPatientDataFromRaw() {
        Map<String, String> data = rawController.getDataFromRaw();
        DataFrame dFrame = new DataFrame("Informacion", 300, 500);
        for(String key: data.keySet()) {
            String nline = key + ": " + data.get(key);
            dFrame.addTextLine(nline);
        }
        dFrame.showWindow();
    }

    public void showRaw(RawImage rawImage){
        ImagePlus raw = rawImage.getFullRaw();
        ImagePlus imagePlus = rawImage.getImages();

        this.imagePanel.displayImage(raw, imagePlus);
        this.widthPanel.setValue(rawImage.getWidth());
        this.heightPanel.setValue(rawImage.getHeight());
        this.offsetPanel.setValue(rawImage.getOffset());
        this.gapPanel.setValue(rawImage.getGapBetweenImages());
    }

    private void extractAndOpenImage(ActionEvent evt) {
        int value;

        JButton button = (JButton) evt.getSource();
        if (button.getName().equals("1")) {
            value = 1;
        } else {
            value = 2;
        }
        rawController.extractImg1(imagePanel.getImg(), value);
    }

    private void extractAndOpenImages(ActionEvent evt) {
    	rawController.extractAndOpenRawImages();
    }

    private void exportarTodo(ActionEvent evt) {
        Optional<RawImage> rawImage = Optional.ofNullable(rawController.getRawImage());
        rawImage.ifPresent(this::exportarPartes);
    }

    private void exportarPartes(RawImage rawImage) {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File("./"));
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fileChooser.setMultiSelectionEnabled(false);
        if(fileChooser.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
            File directory=fileChooser.getSelectedFile();
            Path pathToDir = Paths.get(new StringBuilder(directory.getAbsolutePath()).append("/").append("export").toString());
            directory = new File(pathToDir.toString());
            String[] rawPartsName= new String[]{"Header","Image1.bmp","gap","Image2.bmp","footer"};
            try {
                Files.createDirectories(pathToDir);
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }
            for(int i = 0; i < rawPartsName.length;i++){
                final File finalFile = new File(directory.getPath()+"/"+rawPartsName[i]);
                RawPart rawPart = rawImage.getPart(i);

                if (rawPart.isImagePart()) {
                    saveBMP(rawPart.getInfo(), rawPartsName[i],finalFile,rawImage.getWidth(),rawImage.getHeight());
                } else{
                    try (FileOutputStream fileOutputStream = new FileOutputStream(finalFile)) {
                        fileOutputStream.write(rawPart.getInfo());
                        fileOutputStream.flush();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void saveBMP(byte[] bytes,String title,File f, int imgWidth, int imgHeight){
        ByteProcessor byteProcessor = new ByteProcessor(imgWidth,imgHeight, bytes, null);
        FileSaver fileSaver=new FileSaver(new ImagePlus(title, byteProcessor));
        fileSaver.saveAsBmp(f.getPath());
    }
    
}
