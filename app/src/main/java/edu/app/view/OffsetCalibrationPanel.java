/*
 * Copyright (c) 2020 By Legion. All rights reserved
 *
 * created by Sebastian Fernandez on 2020-06-25 at 19:15
 */
package edu.app.view;

public class OffsetCalibrationPanel extends CalibrationPanel {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public OffsetCalibrationPanel(String text, boolean horizontal) {
        super(text, horizontal);
        createButton(buttonDim, bfont, 0, false, "+Width");

        south.add(createButton(buttonDim, bfont,  200, false, "- 200"), 0);
        south.add(createButton(buttonDim, bfont,  200, true, "+ 200"));
    }
}
