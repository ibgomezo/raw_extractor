package edu.app.view;

import edu.app.controller.RawController;
import java.awt.Dimension;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import lombok.Getter;
import lombok.Setter;

public class DataPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Getter @Setter private JLabel label; 
	@Getter @Setter private JTextField tfield;
	@Getter @Setter private RawController rawProcessor;
		
	public DataPanel(String l, int lg, boolean readOnly) {
		this.label = new JLabel(l+": ");
		this.tfield = new JTextField();
		this.tfield.setColumns(lg);
		this.tfield.setEditable(readOnly);
		this.setPreferredSize(new Dimension(350,30));
		this.add(label);
		this.add(tfield);
	}
	
}
