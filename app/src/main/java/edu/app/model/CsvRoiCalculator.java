package edu.app.model;

import edu.core.roi.RoiCalculator;
import edu.core.util.Utils;
import ij.gui.Roi;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 */
public class CsvRoiCalculator implements RoiCalculator {

    private List<String[]> data;

    private static final String ROI_FORMAT = "\\((\\d+);(\\d+)\\)";
    private static final Pattern PATTERN = Pattern.compile(ROI_FORMAT + " " + ROI_FORMAT);

    public CsvRoiCalculator(){
        data = Utils.ParameterLoader("rois.csv", ",");
    }

    @Override
    public Roi getRoi(String dicomFile) {
        String crudRoi = data.stream().filter(arr ->
                arr[0].equals(dicomFile))
                .findAny()
                .map(arr -> String.valueOf( arr[2])).orElse("");
        Matcher matcher = PATTERN.matcher(crudRoi);
        if(matcher.find()) {
            int x1 = Integer.valueOf(matcher.group(1));
            int y1 = Integer.valueOf(matcher.group(2));
            int x2 = Integer.valueOf(matcher.group(3));
            int y2 = Integer.valueOf(matcher.group(4));
            return new Roi(x1,y1,x2-x1, y2-y1);
        }
        return null;
    }
}
