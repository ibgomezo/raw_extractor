/*
 * Copyright (c) 2020 By Legion. All rights reserved
 *
 * created by Sebastian Fernandez on 2020-07-27 at 21:20
 */
package edu.app.event;

import edu.app.config.ConfigurationModel;
import edu.app.controller.RawController;
import edu.app.view.ConfigurationFrame;
import edu.app.view.MainFrame;
import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.swing.JFileChooser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class UiEventListener {

    @Autowired
    private JFileChooser fileChooser;

    @Autowired
    private RawController rawController;

    @Autowired
    private MainFrame mainFrame;

    @Autowired
    private ConfigurationModel configurationModel;

    private Map<String, Runnable> commandMap = new HashMap<>();

    @PostConstruct
    private void init() {
        commandMap.put(Command.OpenDicom, this::openNewDicomImage);
        commandMap.put(Command.OpenRaw, this::openNewRawImage);
        commandMap.put(Command.OpenConfig, () -> new ConfigurationFrame(configurationModel).showWindow());
    }

    @EventListener
    public void onApplicationEvent(RawUIEvent event) {
        String command = event.getEvent().getActionCommand();
        commandMap.get(command).run();
    }

    private void openNewRawImage() {
        if (fileChooser.showOpenDialog(mainFrame) == JFileChooser.APPROVE_OPTION) {
            File[] files = fileChooser.getSelectedFiles();
            mainFrame.showView(rawController.openRawImage(files[0]));
        }
    }

    private void openNewDicomImage() {
        if (fileChooser.showOpenDialog(mainFrame) == JFileChooser.APPROVE_OPTION) {
            File[] files = fileChooser.getSelectedFiles();
            rawController.loadPatientDataFromDcm(files[0]);
            mainFrame.showView(rawController.openRawFromDicom());
        }
    }
}
