/*
 * Copyright (c) 2020 By Legion. All rights reserved
 *
 * created by Sebastian Fernandez on 2020-07-30 at 19:17
 */
package edu.app.event;

public class Command {
    public static final String OpenRaw = "OpenRaw";
    public static final String OpenDicom = "OpenDicom";
    public static final String OpenConfig = "OpenConfig";
}
