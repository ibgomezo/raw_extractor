/*
 * Copyright (c) 2020 By Legion. All rights reserved
 *
 * created by Sebastian Fernandez on 2020-07-27 at 21:21
 */
package edu.app.event;

import java.awt.event.ActionEvent;
import lombok.Getter;

public class RawUIEvent {
    @Getter
    private ActionEvent event;

    public RawUIEvent(ActionEvent evt) {
        this.event = evt;
    }
}
