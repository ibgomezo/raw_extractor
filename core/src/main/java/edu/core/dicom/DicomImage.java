package edu.core.dicom;

import ij.ImagePlus;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.awt.image.BufferedImage;
import java.awt.Rectangle;

public class DicomImage {

    public final static String SCAN_MODE= "ScanMode";

    private ImagePlus source;

    private static final String standardDcmRegex ="^\\d{4},\\d{4}  ([a-zA-z].*): (.*)$";
    private static final String customDataRegex="^([a-zA-z].*) = (.*)$";
    private static final String xmlRegex ="^<(.*)>(.*)<\\/\\1>";

    private static Set<String> forbidenTags = new HashSet<>();

    static {
       forbidenTags.add("Patient's Name");
       forbidenTags.add("PatientName");
       forbidenTags.add("Patient's Birth Date");
       forbidenTags.add("PatientID");
       forbidenTags.add("Patient ID");
       forbidenTags.add("Patient's Birth Date");
    }

    private HashMap<String,String> tagsMap;

    private String path;

    public DicomImage (ImagePlus source){
        this.source=source;

        String[] tags = this.source.getProperties().get("Info").toString().split("\\n");


        tagsMap = new HashMap<>();
        Arrays.stream(tags)
                .filter(tag -> !processMatcher(standardDcmRegex,tag))
                .filter(tag -> !processMatcher(customDataRegex,tag))
                .filter(tag -> !processMatcher(xmlRegex,tag)).collect(Collectors.toList());
        anonimize();
        anonimizeImage();
    }

    private void anonimize() {
        forbidenTags.forEach(t -> tagsMap.put(t, "Anonimized"));
    }

    private void anonimizeImage() {
        /*
         * Anonimizar la imagen del Dicom es raro... al parecer no se puede hacer
         * desde los metadatos. Por mas que se borren la imagen los muestra igual.
         * Por ejemplo el nombre del paciente
         */
        BufferedImage img = source.getBufferedImage();
        Rectangle[] recs = {
            new Rectangle(169,65,150,120), //Nombre del paciente
            new Rectangle(145,685,220,40) //Nombre de la clínica
        };
        for(Rectangle rec: recs) {
            for(int i=rec.x; i<rec.x + rec.width; i++) {
                for(int j=rec.y; j<rec.y + rec.height; j++) {
                    img.setRGB(i, j, 0);
                }
            }
        }
        source.setImage(img);
        forbidenTags.forEach(t -> tagsMap.put(t, "Anonimized"));
    }

    private boolean processMatcher(String regex, String input){
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(input);
        if(matcher.matches()){
            tagsMap.put(matcher.group(1),matcher.group(2));
            return true;
        }
        return false;
    }

    public String get(String tag){
        return tagsMap.get(tag);
    }
    
    public Set<String> getKeySet(){
    	return tagsMap.keySet();
    }

    public String getFileName(){
        return this.source.getTitle();
    }

    public ImagePlus getSource() {
    	return this.source;
    }

    public void setPath(String path) {
		this.path = path;
	}

    public String getPath() {
		return path;
	}
}
