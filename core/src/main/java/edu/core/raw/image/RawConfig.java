/*
 * Copyright (c) 2020 By Legion. All rights reserved
 *
 * created by Sebastian Fernandez on 2020-07-15 at 19:58
 */
package edu.core.raw.image;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

public class RawConfig {

    @Data
    private static class ConfigValues {
        private int width;
        private int height;
        private int offset;
        private int gap;
    }

    @Setter @Getter
    private boolean isHip;

    private ConfigValues hipValues;
    private ConfigValues spineValues;

    public RawConfig() {
        hipValues = new ConfigValues();
        spineValues = new ConfigValues();
    }


    public int getWidth() {
        return isHip? hipValues.getWidth() : spineValues.getWidth();
    }

    public void setWidth(int width) {
        if(isHip) {
            hipValues.setWidth(width);
        } else {
            spineValues.setWidth(width);
        }
    }



    public int getGap() {
        return isHip? hipValues.getGap() : spineValues.getGap();
    }

    public void setGap(int gap) {
        if(isHip) {
            hipValues.setGap(gap);
        } else {
            spineValues.setGap(gap);
        }
    }

    public int getHeight() {
        return isHip? hipValues.getHeight() : spineValues.getHeight();
    }

    public void setHeight(int height) {
        if(isHip) {
            hipValues.setHeight(height);
        } else {
            spineValues.setHeight(height);
        }
    }

    public int getOffset() {
        return isHip? hipValues.getOffset() : spineValues.getOffset();
    }

    public void setOffset(int offset) {
        if(isHip) {
            hipValues.setOffset(offset);
        } else {
            spineValues.setOffset(offset);
        }
    }
}

