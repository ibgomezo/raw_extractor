package edu.core.raw.image;

import ij.ImagePlus;
import ij.process.ByteProcessor;
import ij.process.ImageProcessor;
import lombok.Getter;
import lombok.Setter;

import java.awt.image.BufferedImage;
import java.util.Arrays;

/**
 * Objeto que representa los archivos RAW
 *
 */

public class RawImage {

    private static final int HEADER = 0;
    private static final int IMAGE_1 = 1;
    private static final int GAP = 2;
    private static final int IMAGE_2 = 3;
    private static final int FOOTER = 4;

    @Getter
    private byte[] bytes;
    @Setter @Getter
    private int width; //Se setea desde la interfaz
    @Setter @Getter
    private int offset; //byte index de la primer imagen
    @Setter @Getter
    private int gapBetweenImages; //byte index de la segunda imagen
    @Setter @Getter
    private int height; //byte index de la segunda imagen

    @Setter @Getter
    private RawConfig rawConfig;

    public RawImage(RawConfig rawConfig, byte[] bytes) {
        updateParams(rawConfig);
        this.bytes = bytes;
        this.rawConfig = rawConfig;
    }

    public RawImage(int width, int offset, int gap, int height, byte[] bytes){
        this.width = width;
        this.offset = offset;
        this.gapBetweenImages = gap;
        this.bytes = bytes;
        this.height = height;
    }

    public BufferedImage getFullImage(){
    	return getFullRaw().getBufferedImage();
    }

    public void updateParams(RawConfig rawConfig) {
        this.width = rawConfig.getWidth();
        this.offset = rawConfig.getOffset();
        this.gapBetweenImages = rawConfig.getGap();
        this.height = rawConfig.getHeight();
    }

    public ImagePlus getImages() {
        ImageProcessor ip1 = getImgProc(offset);
        int start2 = offset + (width * height) + gapBetweenImages;
        if (start2 > bytes.length) {
            start2 = bytes.length;
        }

        ImageProcessor ip2 = getImgProc(start2);

        ImagePlus imp = new ImagePlus("", ip1);
        imp.setStack(imp.getStack());
        imp.getStack().addSlice("Img2", ip2);

        return imp;
    }

    private ImageProcessor getImgProc(int startingPoint) {
        int totalBytes = bytes.length - startingPoint;
        if (totalBytes < 0) {
            totalBytes = 0;
        }
        int size = width *  height;
        byte[] imgArray = new byte[size];
        System.arraycopy(bytes, startingPoint, imgArray, 0, Math.min(size, totalBytes));
        return new ByteProcessor(width, imgArray.length / width, imgArray, null);
    }

    public ImagePlus getFullRaw() {
        int imageHeigth = bytes.length / width;
        int imageEnd = width * imageHeigth;
        byte[] imgArray = Arrays.copyOfRange(bytes, 0, imageEnd);
        return new ImagePlus("", new ByteProcessor(width, imageHeigth, imgArray, null));
    }

    public byte[] getBytesFromImage(Integer begin, Integer end) {
    	return Arrays.copyOfRange(bytes, begin, end);
    }
 
    public byte[] getBytesFromImageOne(int height, int offset) {
    	int begin = offset;
    	int end = begin + width * height;
    	return getBytesFromImage(begin, end);
    }
    
    public byte[] getBytesFromImageTwo(int height, int offset, int gap) {
    	int begin = offset + (width * height) + gap;
    	int end = begin + width * height;
    	return getBytesFromImage(begin, end);
    }


    public RawPart getPart(int i) {
        if (i < HEADER || i > FOOTER) {
            throw  new IllegalArgumentException("Not a valid Part");
        }

        int startingPoint = 0;
        if(i == HEADER) {
            return new RawPart(Arrays.copyOfRange(bytes, startingPoint, offset), false);
        }

        startingPoint += offset;
        if(i == IMAGE_1) {
            return new RawPart(Arrays.copyOfRange(bytes, startingPoint, startingPoint + width * height),true);
        }

        startingPoint += width * height;
        if(i == GAP) {
            return new RawPart(Arrays.copyOfRange(bytes, startingPoint, startingPoint + gapBetweenImages), false);
        }

        startingPoint += gapBetweenImages;
        if(i == IMAGE_2) {
            return new RawPart(Arrays.copyOfRange(bytes, startingPoint, startingPoint + width * height), true);
        }

        startingPoint += width * height;
        return new RawPart(Arrays.copyOfRange(bytes, startingPoint, bytes.length), false);
    }

    public static class RawPart {
        @Getter
        private boolean imagePart;

        @Getter
        private byte[] info;

        public RawPart(byte[] info, boolean imagePart){
            this.info = info;
            this.imagePart = imagePart;
        }

    }
}
