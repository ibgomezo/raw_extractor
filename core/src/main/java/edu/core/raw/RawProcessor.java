package edu.core.raw;

import edu.core.dicom.DicomImage;
import edu.core.dxa.DXAImage;
import edu.core.raw.image.RawConfig;
import edu.core.raw.image.RawImage;
import edu.core.util.AppProperties;
import edu.core.util.Utils;
import ij.plugin.DICOM;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.Hashtable;
import java.util.List;
import java.util.Optional;
import javax.annotation.PostConstruct;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RawProcessor {


	@Autowired
	AppProperties properties;

	@Getter
	private File file;

	@Getter
	private RawConfig rawConfig;

	@Getter
	private DXAImage dxaImage;

	@PostConstruct
    public void init() {
        rawConfig = createConfig();
        rawConfig.setHip(true);
    }

	public Hashtable<String, String> getDataFromRaw() {
		List<String[]> data = Utils.ParameterLoader("data.csv", ",");
		Hashtable<String, String> result = new Hashtable<>();
		try (final RandomAccessFile randomAccessFile = new RandomAccessFile(this.file, "r")) {
			for(String[] o: data) {
				randomAccessFile.seek(Integer.parseInt(o[1]));
				byte[] bytes = new byte[Integer.parseInt(o[2])];
				randomAccessFile.read(bytes);
				result.put(o[0], new String(bytes));
			}
			return result;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void setConfig(String preposition, RawConfig rawConfig) {
		rawConfig.setWidth(properties.getProperty(preposition, AppProperties.WIDTH));
		rawConfig.setHeight(properties.getProperty(preposition, AppProperties.HEIGHT));
		rawConfig.setOffset(properties.getProperty(preposition, AppProperties.OFFSET));
		rawConfig.setGap(properties.getProperty(preposition, AppProperties.GAP));
	}

	public RawConfig createConfig() {
		RawConfig rawConfig = new RawConfig();

		rawConfig.setHip(true);
		setConfig("hip_", rawConfig);

		rawConfig.setHip(false);
		setConfig("vert_", rawConfig);

		return rawConfig;
	}

	public void updateRaw() {
		dxaImage.updateConfig();
	}

	public RawImage getRawImage() {
		if (dxaImage.getRawImage() == null) {
			dxaImage.setRawFile(file);
        }
		dxaImage.updateConfig();
        return dxaImage.getRawImage();
	}

	public void setFile(File file) {
		if (file != this.file) {
			this.file = file;
			openRaw();
		}
	}

	public void openDicom(DICOM dicom) {
		if (dxaImage == null) {
            dxaImage = new DXAImage(dicom, rawConfig);
        } else {
		    dxaImage.updateDicom(dicom);
        }
	}

	private void openRaw() {
		if (dxaImage == null) {
			dxaImage = new DXAImage(file, rawConfig);
		} else {
			dxaImage.setRawFile(file);
		}
	}

    public DicomImage getDicomImage() {
		return Optional.ofNullable(dxaImage).map(DXAImage::getDicomImage).orElse(null);
	}
}