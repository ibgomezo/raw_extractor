/*
 * Copyright (c) 2020 By Legion. All rights reserved
 *
 * created by Sebastian Fernandez on 2020-09-02 at 20:21
 */
package edu.core.raw.process.solver;

import edu.core.dicom.DicomImage;
import org.springframework.stereotype.Component;

@Component
public class DefaultRawNameResolver implements RawNameResolver {

    static final String SCAN_TAG = "Scan";

    @Override
    public String resolve(DicomImage dicomImage)  {

        String scanValue = dicomImage.get(SCAN_TAG);

        String letter = scanValue.substring(scanValue.length()-12, scanValue.length()-11);
        String year = scanValue.substring(scanValue.length()-7, scanValue.length()-5);
        String month = scanValue.substring(scanValue.length()-11, scanValue.length()-9);
        month = Integer.toHexString(Integer.parseInt(month)).toUpperCase();
        String day = scanValue.substring(scanValue.length()-9, scanValue.length()-7);
        String ext = ".P" + scanValue.substring(scanValue.length()-5, scanValue.length()-3);

        return new StringBuilder("P")
            .append(letter)
            .append(year)
            .append(month)
            .append(day)
            .append(letter)
            .append(ext)
            .toString();
    }
}
