/*
 * Copyright (c) 2020 By Legion. All rights reserved
 *
 * created by Sebastian Fernandez on 2020-09-02 at 20:18
 */
package edu.core.raw.process.solver;

import edu.core.dicom.DicomImage;

public interface RawNameResolver {

    String resolve(DicomImage dicomImage);
}
