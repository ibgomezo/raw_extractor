/*
 * Copyright (c) 2020 By Legion. All rights reserved
 *
 * created by Sebastian Fernandez on 2020-09-03 at 18:58
 */
package edu.core.raw.process.algorithm;

import java.util.List;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AlgorithmConfig {

    @Autowired
    @Getter
    private List<Algorithm> algorithms;
}
