package edu.core.raw.process.algorithm;

import ij.process.ByteProcessor;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import java.awt.Rectangle;

import org.springframework.stereotype.Component;

import ij.ImagePlus;
import ij.gui.Roi;
import java.awt.image.BufferedImage;

/*
    | 1	 2	1 |
G =	| 2	 4	2 |
    | 1	 2	1 |
*/
@Component
public class GaussianBlur extends Algorithm {
    static int[] gauss = {1,1,1,0,0,0,-1,-1,-1};

    public ImagePlus apply(ImagePlus original, ImagePlus sister, Object ...params) {
        ImagePlus copy = new ImagePlus("Convoluted Copy",
            original.getProcessor().duplicate());

        copy.setRoi(original.getRoi());
        copy.getProcessor().convolve3x3(gauss);
        copy.deleteRoi();

        return  copy;
    }
}
