package edu.core.raw.process.algorithm;

import java.util.Observable;

import ij.ImagePlus;

public abstract class Algorithm extends Observable {
	private Integer progress;
	
	protected void setProgress(int value) {
		this.progress = value < 0 ? 0 : value;
		this.progress = value > 100 ? 100 : value;
		setChanged();
		notifyObservers(this.progress);
	}
	
	public int getProgress() {
		return progress;
	}
	
	public abstract ImagePlus apply(ImagePlus lowEnergy, ImagePlus highEnergy, Object ...params);

	public String getName() {
		return this.getClass().getSimpleName();
	}
}
