/*
 * Copyright (c) 2020 By Legion. All rights reserved
 *
 * created by Sebastian Fernandez on 2020-08-23 at 16:54
 */
package edu.core.raw.process.algorithm;

import ij.ImagePlus;
import ij.process.ByteProcessor;
import org.springframework.stereotype.Component;

@Component
public class BoneExtraction extends Algorithm {

    private static int LOW_MULT = 2;
    private static int HIGH_MULT = 3;

    @Override
    public ImagePlus apply(ImagePlus low, ImagePlus high, Object... params) {

        ImagePlus original = low;
        ImagePlus other = high;

        byte[] img1 = (byte [])original.getProcessor().getPixels();
        byte[] img2 = (byte [])other.getProcessor().getPixels();

        byte[] resultInt = new byte[img1.length];

        int min = Integer.MAX_VALUE, max = 0;
        for (int i = 0; i< img1.length; i++) {
            int v = Byte.toUnsignedInt((byte)img1[i]) * 2 - Byte.toUnsignedInt((byte)img2[i]) * 3;
           // v =  img2[i] * HIGH_MULT - img1[i] * LOW_MULT;

            min = min > v ? v : min;
            max = max < v ? v : max;

            resultInt[i] = (byte)v;
        }

        double rate = ((double)(max -min))/255d;

        byte[] resultByte = new byte[resultInt.length];

        /*for (int i = 0 ; i < resultInt.length; i++) {
            resultByte[i] = (byte) (((int)resultInt[i] * rate));
        }*/

        return new ImagePlus("Substraction Result",
            new ByteProcessor(original.getWidth(), original.getHeight(), resultInt));

        //new PreviewFrame(, , null);
    }
}
