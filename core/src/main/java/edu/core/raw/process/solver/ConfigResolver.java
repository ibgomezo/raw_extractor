/*
 * Copyright (c) 2020 By Legion. All rights reserved
 *
 * created by Sebastian Fernandez on 2020-09-03 at 19:29
 */
package edu.core.raw.process.solver;

import edu.core.dxa.DXAImage;
public interface ConfigResolver {

    /**
     * Edits the dxaImage's config
     * @param dxaImage sourceImage
     */
    void setConfig(DXAImage dxaImage);
}
