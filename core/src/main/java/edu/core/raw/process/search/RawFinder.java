/*
 * Copyright (c) 2020 By Legion. All rights reserved
 *
 * created by Sebastian Fernandez on 2020-07-23 at 18:52
 */
package edu.core.raw.process.search;

import edu.core.dicom.DicomImage;
import java.io.File;

public interface RawFinder {

    File openRawFromDicom(DicomImage dicomImage, String folder);


}
