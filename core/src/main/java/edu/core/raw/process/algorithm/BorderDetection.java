package edu.core.raw.process.algorithm;

import ij.ImagePlus;
import ij.process.ColorProcessor;
import ij.process.ImageProcessor;
import ij.process.ShortProcessor;
import java.awt.image.ColorModel;
import org.springframework.stereotype.Component;

@Component
public class BorderDetection extends Algorithm {
    public ImagePlus apply(ImagePlus original, ImagePlus sister, Object ...params) {
        ImagePlus copy = new ImagePlus("Deteccion de bordes",
            original.getProcessor().duplicate());

        copy.setRoi(original.getRoi());

        ImageProcessor ipBorder = copy.getProcessor().duplicate();
        ipBorder.findEdges();

        for(int x =0; x < copy.getWidth(); x++) {
            for (int y =0; y <copy.getHeight(); y++) {
                int p1 = ipBorder.getPixel(x, y);
                int finalpx = copy.getProcessor().getPixel(x, y);
                if (copy.getRoi() == null || copy.getRoi().contains(x,y)) {
                    finalpx = p1;
                }
                copy.getProcessor().set(x, y, finalpx);
               // finalPixels[y*copy.getWidth() + x] = finalpx;
            }
        }

        copy.deleteRoi();

        return  copy;
    }
}
