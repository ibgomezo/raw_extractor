package edu.core.raw.process.algorithm;

import edu.core.util.Utils;
import ij.ImagePlus;
import java.awt.Point;
import java.awt.Rectangle;
import java.util.List;
import org.springframework.stereotype.Component;

/**
 * La presente clase intenta demostrar la manera que tiene la herramienta para aplicar algoritmos
 * a las imagenes extraidas desde un archivo RAW
 **/
@Component
public class RoiMapper extends Algorithm {

	public RoiMapper() {
		// TODO Auto-generated constructor stub
	}

	@Override
	/**
	 * La imagen recibida como parámetro puede tener una imagen hermana, que es la
	 * producida por el otro rayo de diferente frecuencia en el densitómetro.
	 * Aquí esa imagen se llama sister
	 **/
	public ImagePlus apply(ImagePlus original, ImagePlus sister, Object... params) {
		Point[] points = getRoiFromFile((String)params[0]);
		if (points.length > 0) {
			Point p1 = points[0];
			Point p2 = points[1];
			original.setRoi(new Rectangle(p1.x, p1.y, p2.x - p1.x, p2.y - p1.y));
			return original;
		}
		return null;
	}
	
	private Point[] getRoiFromFile(String rawPath) {
		String filename = rawPath.substring(rawPath.lastIndexOf('/') +1, rawPath.length());
		
		List<String[]> rois = Utils.ParameterLoader("rois.csv", ",");
		for(String[] row: rois) {
			if(row[1].equals(filename)) {
				return toPoints(row[2]);
			}
		}
		Point[] r = {};
		return  r;
	}
	
	private Point[] toPoints(String s) {
		int x1 = Integer.parseInt(s.substring(1, s.indexOf(';')));
		int y1 = Integer.parseInt(s.substring(s.indexOf(';') +1, s.indexOf(')')));
		
		int x2 = Integer.parseInt(s.substring(s.lastIndexOf('(') +1, s.lastIndexOf(';')));
		int y2 = Integer.parseInt(s.substring(s.lastIndexOf(';') +1, s.lastIndexOf(')')));
		Point[] result = {new Point(x1,y1), new Point(x2,y2)};
		return result;
	}
}
