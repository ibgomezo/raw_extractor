/*
 * Copyright (c) 2020 By Legion. All rights reserved
 *
 * created by Sebastian Fernandez on 2020-09-03 at 19:36
 */
package edu.core.raw.process.solver;

import edu.core.dxa.DXAImage;
import edu.core.util.AppProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Component;

@Component
@Lazy
public class DefaultConfigResolver implements ConfigResolver {

    public static final String PART_EXAMINED = "Body Part Examined";
    public static final String HIP_VALUE = "HIP";
    @Autowired
    AppProperties properties;



    @Override
    public void setConfig(DXAImage dxaImage) {
        boolean isHip = dxaImage.getDicomImage().get(PART_EXAMINED).contains(HIP_VALUE);
        String prep = isHip? AppProperties.HIP : AppProperties.VERT;

        dxaImage.getRawConfig().setHip(isHip);
        dxaImage.getRawConfig().setWidth(properties.getProperty(prep, AppProperties.WIDTH));
        dxaImage.getRawConfig().setHeight(properties.getProperty(prep, AppProperties.HEIGHT));
        dxaImage.getRawConfig().setOffset(properties.getProperty(prep, AppProperties.OFFSET));
        dxaImage.getRawConfig().setGap(properties.getProperty(prep, AppProperties.GAP));
    }
}
