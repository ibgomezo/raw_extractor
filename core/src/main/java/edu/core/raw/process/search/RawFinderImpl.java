/*
 * Copyright (c) 2020 By Legion. All rights reserved
 *
 * created by Sebastian Fernandez on 2020-07-23 at 18:52
 */
package edu.core.raw.process.search;

import edu.core.dicom.DicomImage;
import edu.core.raw.process.solver.RawNameResolver;
import java.io.File;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RawFinderImpl implements RawFinder {

    @Autowired
    private RawNameResolver rawNameResolver;


    @Override
    public File openRawFromDicom(DicomImage dicomImage, String folder) {
        return searchRecursivelly(folder, rawNameResolver.resolve(dicomImage));
    }

    private File searchRecursivelly(String folder, String fileName) {
        File current = new File(folder);
        File[] files = current.listFiles();
        if (files == null) {
            return null;
        }
        for(File file: files) {
            if(file.getName().equals(fileName)) {
                return file;
            } else {
                if(file.isDirectory()) {
                    File result = searchRecursivelly(file.getAbsolutePath(), fileName);
                    if (result != null) {
                        return result;
                    }
                }
            }
        }
        return null;
    }


}
