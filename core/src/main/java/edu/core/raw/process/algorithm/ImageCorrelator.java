package edu.core.raw.process.algorithm;

import ij.IJ;
import ij.ImagePlus;
import ij.ImageStack;
import ij.gui.Roi;
import ij.measure.CurveFitter;
import ij.process.FloatProcessor;
import ij.process.ImageProcessor;
import ij.text.TextWindow;
import ij.util.Tools;
import java.awt.Checkbox;
import java.awt.Label;
import java.awt.TextField;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JTextField;
import org.springframework.stereotype.Component;

@Component
public class ImageCorrelator extends Algorithm {
    static int[] kernelX = new int[]{-1,0,1,-2,0,2,-1,0,1};
    static int[] kernelY = new int[]{1,2,1,0,0,0,-1,-2,-1};
    //private static boolean dR2=true, dEquation=true, dResults=true, fixSlide=false, nextSlide=false, cMap=true, cArray=false, cPlot=true;

    private int nSlices=0; //msize es el valor de la matriz de correlacion (3)


    public static  int cStat;

    public double[] plotCorrelation(ImageStack img1, ImageStack img2, int cSlide) {
        int nSlicesImg1 = img1.getSize();


        ImageProcessor ip1, ip2;

            for(int i=1; i<=nSlicesImg1; i++) {
                IJ.showProgress((double)(i)/nSlices);
                IJ.showStatus("a: "+(i)+"/"+nSlices);

                //Crear Imagen 2
                ip2 = img2.getProcessor(i);
                ip2 = ip2.convertToFloat();

                //Usar imagen 1
                ip1 = img1.getProcessor(i);
                ip1 = ip1.convertToFloat();

                float[] ip1Array = getNewArray(ip1, 3,cStat);
                float[] ip2Array = getNewArray(ip2, 3,cStat);
                double[] cStats= getCorrValues(ip1Array, ip2Array);
                writeResults(cStats);
                return cStats;
            }
        return null;
    //
    }

    double[] getCorrValues(float[] a1, float[] a2){
        double[] d=new double[3];
        d[2]= getR(a1,a2);
        String sLabel="", R2label="";
        CurveFitter cf = new CurveFitter(Tools.toDouble(a1), Tools.toDouble(a2));
        cf.doFit(CurveFitter.STRAIGHT_LINE);
        double[] s = cf.getParams();
        d[0]=s[0];d[1]=s[1];
        return d;
    }




    String[] writeResults(double[] l){
        return  new String[] {
            IJ.d2s(l[2],2),IJ.d2s(l[1],2), IJ.d2s(l[0],2)};
    }

    public float[] getNewArray(ImageProcessor ipTemp, int mSize, int stat){
        int ww=ipTemp.getWidth();
        int hh=ipTemp.getHeight();
        int nY=(int)(hh/mSize);
        int nX=(int)(ww/mSize);
        float[] array= new float[nX*nY];
        float[] localArea= new float[mSize*mSize];
        int counter=0;
        for (int i = 0;i<nY;i++){
            int yStep=i*mSize;

            for (int ii = 0;ii<nX;ii++){
                int xStep=ii*mSize;
                float mValue = 0;
                int index =0;
                //Accumulates stats in each subimages
                for (int y = yStep; y < (yStep+mSize); y++){
                    for (int x = xStep; x < (xStep+mSize); x++){
                        localArea[index]=ipTemp.getPixelValue(x,y);
                        index++;
                    }
                }
                if (stat==4){
                    FloatProcessor temp= new FloatProcessor(mSize,mSize,localArea,null);
                    array[counter] = getFacetDetails(temp);
                }
                else array[counter] = getStatistics(localArea,mSize,stat);
                counter++;
            }
        }
        return array;
    }

    float getR(float[] d1, float[] d2){
        float t1=0, t2=0, sum=0;
        float xMean=getMean(d1);
        float yMean=getMean(d2);
        float xStd=getStd(xMean, d1);
        float yStd=getStd(yMean, d2);
        for (int i=0; i<d1.length;i++){
            t1=(d1[i]-xMean)/xStd;
            t2=(d2[i]-yMean)/yStd;
            sum=sum+(t1*t2);
        }
        float r= sum/(d1.length-1);
        return (r);
    }

    //Based on the  Facetorientation plugin by Bob Dougherty
    public float getFacetDetails(ImageProcessor ipTemp){
        int ww=ipTemp.getWidth();
        int hh=ipTemp.getHeight();
        int ps=1;	//pixelSize, no calibration

        FloatProcessor fSlice;
        float[] gradX = (float[])getPixelValues(ipTemp);
        float[] gradY = (float[])getPixelValues(ipTemp);
        //sArea=0;
        float C=0,S=0,R=0,aT=0;
        //Put the pixel arrays into image processors for the convolutions.
        ImageProcessor ipGradX = new FloatProcessor(ww,hh);
        ipGradX.setPixels(gradX);
        ImageProcessor ipGradY = new FloatProcessor(ww,hh);
        ipGradY.setPixels(gradY);

        //Evaluate the gradients.
        ipGradX.convolve3x3(kernelX);
        ipGradY.convolve3x3(kernelY);
        float[] phi = new float[ww*hh];

        //RPD limits changed in the loops below
        for (int y = 1; y < (hh-1); y++){
            for (int x = 1; x < (ww-1); x++){
                int index = x + ww*y;
                //The derivative requires the factors 1/(2*dx) for the centered
                //finite difference and 1/4 for the coefficients of the kernel.
                double gx = gradX[index]/(8*ps);
                double gy = gradY[index]/(8*ps);
                phi[index] = (float)(Math.atan2(gy,gx)+Math.PI);
                //Calculates the mean resultant vector (R)
                //See Tovey et al., 1992
                //RPD Use double angles to avoid cancellation
                C+= Math.cos(2*phi[index]);
                S+= Math.sin(2*phi[index]);
                phi[index]=(float)(Math.toDegrees(phi[index]));if (phi[index]<0) phi[index]=360+phi[index];
            }
        }
        C=C/((hh-2)*(ww-2));S=S/((hh-2)*(ww-2));
        R=(float)(Math.sqrt(sqr2(C)+sqr2(S)));
        //RPD
        //Undo double angle									   90
        aT = (float)(Math.toDegrees(Math.atan2(S,C)/2));
        if (aT<0) aT=90-Math.abs(aT); 			//angles given between 0 and 180, where 180  +   0
        else aT=aT+90;
        return aT;
    }

    private float[] getPixelValues(ImageProcessor ipTemp){
        int ww=ipTemp.getWidth();
        int hh=ipTemp.getHeight();
        float[] pValues = new float[ww*hh];
        for (int y = 0; y < hh; y++){
            for (int x = 0; x < ww; x++){
                int index = x + ww*y;
                pValues[index]=ipTemp.getPixelValue(x,y);
            }
        }
        return pValues;
    }

    public float getStatistics(float[] ipTemp, int ww, int p){
        int hh=ww;//ipTemp.getHeight();
        int N=ww*hh;
        float min = Float.MAX_VALUE;
        float max = -Float.MAX_VALUE;
        float[] rValues = new float[7];
        float ra = 0, rq = 0, sk = 0, ku = 0;
        float zMean=getMean(ipTemp);
        float zStd = getStd(zMean,ipTemp);
        int index=0;
        for (int y = 0; y < hh; y++){
            for (int x = 0; x < ww; x++){
                //float temp = ipTemp.getPixelValue(x,y);
                float temp = ipTemp[index];
                float zTemp=temp-zMean;
                sk += sqr3(zTemp/zStd);
                ku += sqr4(zTemp/zStd);
                ra += Math.abs(zTemp);
                if (temp<min) min = temp;
                if (temp>max) max = temp;
                index++;
            }
        }
        // Calculate Roughness stats (Ra, Rq, Rsk, Rku)
        rValues[0] = (float)(zMean);
        rValues[1] = (float)(zStd);
        rValues[2] = (float)(sk/N);
        rValues[3] = (float)((ku/N)-3);
        rValues[4] = min;
        rValues[5] = max;
        rValues[6] = max-Math.abs(min);
        return rValues[p];
    }

    double sqr2(double x) {return x*x;}
    double sqr3(double x) {return x*x*x;}
    double sqr4(double x) {return x*x*x*x;}

    double sqr(double x) {return x*x;}

    float getMean(float[] dataset){
        double mValue=0;
        for (int j=0; j<dataset.length; j++){mValue += dataset[j];}
        return (float) (mValue/dataset.length);
    }

    float getStd(float mValue,float[] dataset){
        float sValue=0;
        if (dataset.length==1) {return  (sValue);}
        else{
            for (int j=0; j<dataset.length; j++){sValue += sqr(mValue-dataset[j]);}
            return (float) (Math.sqrt(sValue/(dataset.length-1)));
        }
    }

    @Override
    public ImagePlus apply(ImagePlus img1, ImagePlus img2, Object ...params) {
    	setProgress(0);

        ImagePlus original = img1;
        ImagePlus other = img2;

        Roi imgRoi = (Roi) params[0];
        original.setRoi(imgRoi);

        ImagePlus smaller = new ImagePlus( "Cropped Dicom", original.getProcessor().crop());

        ImagePlus bigger;
        if (other.getWidth() >= smaller.getWidth() && other.getHeight()>= smaller.getHeight()){
            bigger = other;
        } else {
            bigger = smaller;
            smaller = other;
        }

        Roi roi = new Roi(0, 0, smaller.getWidth(), smaller.getHeight());

        List<List<String>> corrs = new ArrayList<>();
        ImageProcessor processor = bigger.getProcessor();
        processor.setRoi(roi);
        double max = 0.0;
        int x = 0;
        int y = 0;

        int diff = bigger.getWidth() - smaller.getWidth();
        int diff2 = bigger.getHeight() - smaller.getHeight();

        int total = diff * diff2;
        total = total == 0? 1: total;
        int it = 1;

        for (int i= 0; i <= (bigger.getWidth() - smaller.getWidth());i++ ) {
            for (int j = 0; j <= (bigger.getHeight() - smaller.getHeight()); j++ ){
                it++;

                roi.setLocation(i,j);
                processor.setRoi(roi);
                ImagePlus cc = new ImagePlus("", processor.crop());
                double[] localCorr = plotCorrelation(cc.getImageStack(), smaller.getImageStack(), 0);
                if (max < localCorr[2]) {
                    max = localCorr[2];
                    x = i;
                    y = j;
                }
                List<String> corr = Arrays.asList(writeResults(localCorr));
                corrs.add(corr);
            }
            setProgress((it*100)/total);
        }
        setProgress(100);
        roi.setLocation(x, y);

        processor.setRoi(roi);

        return new ImagePlus("Correlation Result", processor.crop());
    }
}

