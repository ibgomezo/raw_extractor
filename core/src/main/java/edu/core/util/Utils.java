package edu.core.util;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.imageio.ImageIO;

import ij.ImagePlus;
import ij.io.FileInfo;

public final class Utils {

	private Utils() {
		//avoid instantiate
	}
	
	public static FileInfo fileInfoFromFile(File file, int width, int height, int offset) {
		final FileInfo fileInfo = new FileInfo();
		fileInfo.directory = file.getParent();
		fileInfo.fileName =  file.getName();
		fileInfo.width = width;
		fileInfo.height = height;
		fileInfo.offset = offset;
		return fileInfo;
	}
	
	public static List<String[]> ParameterLoader(String file, String separator) {
		BufferedReader reader = null;
	    String line;
	    List<String[]> result = new ArrayList<>();
		try {
			reader = new BufferedReader(new FileReader(file));
			while ((line = reader.readLine()) != null) {
				String[] values = line.split(separator);
				result.add(values);
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}  catch (IOException e) {
			e.printStackTrace();
	    } finally {
	        if (reader != null) {
	            try {
	                reader.close();
	            } catch (IOException e) {
	                e.printStackTrace();
	            }
	        }
	    }
		return result;
	}
	
    /**

     * Scans all classes accessible from the context class loader which belong to the given package and subpackages.

     *

     * @param packageName The base package

     * @return The classes

     * @throws ClassNotFoundException

     * @throws IOException

     */

    public static Class[] getClasses(String packageName) throws ClassNotFoundException, IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;
        String path = packageName.replace('.', '/');
        Enumeration<URL> resources = classLoader.getResources(path);
        List<File> dirs = new ArrayList<File>();
        while (resources.hasMoreElements()) {
            URL resource = resources.nextElement();
            dirs.add(new File(resource.getFile()));
        }

        ArrayList<Class> classes = new ArrayList<Class>();
        for (File directory : dirs) {
            classes.addAll(findClasses(directory, packageName));
        }

        return classes.toArray(new Class[classes.size()]);
    }

    /**

     * Recursive method used to search all classes in a given directory and subdirs.

     *

     * @param directory   The base directory

     * @param packageName The package name for classes found inside the base directory

     * @return The classes

     * @throws ClassNotFoundException

     */

    private static List<Class> findClasses(File directory, String packageName) throws ClassNotFoundException {
        List<Class> classes = new ArrayList<Class>();
        if (!directory.exists()) {
            return classes;
        }

        File[] files = directory.listFiles();

        for (File file : files) {
            if (file.isDirectory()) {
                assert !file.getName().contains(".");
                classes.addAll(findClasses(file, packageName + "." + file.getName()));
            } else if (file.getName().endsWith(".class")) {
                classes.add(Class.forName(packageName + '.' + file.getName().substring(0, file.getName().length() - 6)));
            }
        }

        return classes;
    }
    
    public static byte[] getBytesFromImagPlus(ImagePlus img) throws IOException{
		BufferedImage originalImage =  img.getBufferedImage(); 				
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		ImageIO.write( originalImage, "jpg", baos );
		baos.flush();
		byte[] imageInByte = baos.toByteArray();
		baos.close();
		return imageInByte;	
    }	
}
