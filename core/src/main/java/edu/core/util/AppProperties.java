package edu.core.util;

import java.util.Map;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Controller;

@ConfigurationProperties(prefix = "param.raw")
@Controller
public class AppProperties {

    public static final String WIDTH = "width";
    public static final String HEIGHT = "height";
    public static final String OFFSET = "offset";
    public static final String GAP = "gap";


    public static final String HIP = "hip_";
    public static final String VERT = "vert_";

    @Getter @Setter
    private Map<String, Integer> hip;

    @Getter @Setter
    private Map<String, Integer> vert;


    private Map<String, Integer> getPrep(String key) {
        switch (key){
            case HIP:
                return hip;
            case VERT:
                return vert;
        }
        return hip;
    }

    public Integer getProperty(String prep, String key) {
        return  getPrep(prep).get(key);
    }
}
