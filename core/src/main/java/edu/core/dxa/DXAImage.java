package edu.core.dxa;

import edu.core.dicom.DicomImage;
import edu.core.raw.image.RawConfig;
import edu.core.raw.image.RawImage;
import ij.gui.Roi;
import ij.plugin.DICOM;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import lombok.Getter;
public class DXAImage {

    @Getter
    private RawImage rawImage;
    @Getter
    private DicomImage dicomImage;
    @Getter
    private Roi roi;
    @Getter
    private RawConfig rawConfig;

    public DXAImage (DicomImage dicomImage, RawImage rawImage, RawConfig rawConfig){
        this.dicomImage = dicomImage;
        this.rawImage = rawImage;
        this.rawConfig = rawConfig;
    }

    public DXAImage (File file, RawConfig rawConfig) {
        this.rawConfig = rawConfig;
        setRawFile(file);
    }

    public DXAImage(DICOM dicom, RawConfig rawConfig) {
        dicomImage = new DicomImage(dicom);
        this.rawConfig = rawConfig;
    }

    public void updateDicom(DICOM dicom) {
        dicomImage = new DicomImage(dicom);
        rawImage = null;
    }

    public void setRawConfig(RawConfig rawConfig) {
        this.rawConfig = rawConfig;
        if (rawImage != null) {
            rawImage.updateParams(rawConfig);
        }
    }

    public void setRawFile(File file) {
        try (final RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw")) {
            //header
            long fileLenth = randomAccessFile.length();
            int realLength = (fileLenth>Integer.MAX_VALUE) ? Integer.MAX_VALUE : (int) fileLenth;

            byte [] imageBytes = new byte[realLength];
            randomAccessFile.read(imageBytes);

            this.rawImage = new RawImage(rawConfig, imageBytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void updateConfig() {
        this.getRawImage().updateParams(rawConfig);
    }

}
