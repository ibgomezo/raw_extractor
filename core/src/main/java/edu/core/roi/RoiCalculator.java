package edu.core.roi;

import ij.gui.Roi;

public interface RoiCalculator {

    Roi getRoi(String dicomFile);
}
